-- MySQL Script generated by MySQL Workbench
-- 04/19/16 20:18:21
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema learnit
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema learnit
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `learnit` DEFAULT CHARACTER SET utf8 ;
USE `learnit` ;

-- -----------------------------------------------------
-- Table `learnit`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`user` (
  `user_id` NVARCHAR(1000) NOT NULL,
  `first_name` NVARCHAR(100) NULL,
  `last_name` NVARCHAR(100) NULL,
  `password` NVARCHAR(100) NULL,
  `phone_number` NVARCHAR(100) NULL,
  `username` NVARCHAR(100) NULL,
  `gender` VARCHAR(3) NULL,
  `birthday` DATE NULL,
  `isEnable` BIT(1) NULL,
  `isLock` BIT(1) NULL,
  `about` NVARCHAR(1500) NULL,
  `image` NVARCHAR(100) NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`role` (
  `role_id` INT NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(100) NULL,
  `description` NVARCHAR(1000) NULL,
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`certification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`certification` (
  `certification_id` INT NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(1000) NULL,
  `center` NVARCHAR(1000) NULL,
  `valid_from` DATE NULL,
  `valid_to` DATE NULL,
  `description` NVARCHAR(1000) NULL,
  `isDelete` BIT(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`certification_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`language` (
  `language_id` INT NOT NULL AUTO_INCREMENT,
  `language_value` NVARCHAR(100) NULL,
  `isDelete` NVARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`language_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`experience`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`experience` (
  `experience_id` INT NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(100) NULL,
  `company` NVARCHAR(150) NULL,
  `date_from` DATE NULL,
  `date_to` DATE NULL,
  `description` VARCHAR(45) NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`experience_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`address` (
  `street` NVARCHAR(1000) NOT NULL,
  `city` NVARCHAR(1000) NULL,
  `province` NVARCHAR(1000) NULL,
  `country` NVARCHAR(100) NULL,
  `post_code` VARCHAR(45) NULL,
  PRIMARY KEY (`street`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`course` (
  `courses_id` INT NOT NULL AUTO_INCREMENT,
  `image` NVARCHAR(100) NULL,
  `title` NVARCHAR(1000) NULL,
  `description` NVARCHAR(1500) NULL,
  `requirement` NVARCHAR(1500) NULL,
  `goals_course` NVARCHAR(1500) NULL,
  `target_audience` NVARCHAR(1500) NULL,
  `cost_course` DOUBLE NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`courses_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`section`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`section` (
  `section_id` INT NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(1000) NULL,
  `goals_section` NVARCHAR(1500) NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`section_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`lecture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`lecture` (
  `lecture_id` INT NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(1000) NULL,
  `duration_video` NVARCHAR(100) NULL,
  `link_video` NVARCHAR(1000) NULL,
  `description` NVARCHAR(1500) NULL,
  `source_code` NVARCHAR(1000) NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`lecture_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`rate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`rate` (
  `rate_id` INT NOT NULL AUTO_INCREMENT,
  `vote` NVARCHAR(100) NULL,
  `comment` NVARCHAR(1000) NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`rate_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`comment` (
  `comment_id` INT NOT NULL AUTO_INCREMENT,
  `comment` NVARCHAR(1500) NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`comment_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`skill_level`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`skill_level` (
  `level_id` INT NOT NULL AUTO_INCREMENT,
  `level_value` NVARCHAR(100) NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`level_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`link_social`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`link_social` (
  `linksocial_id` INT NOT NULL AUTO_INCREMENT,
  `social_value` NVARCHAR(100) NULL,
  `isDelete` VARCHAR(1) NULL,
  `delete_date` DATE NULL,
  `create_date` DATE NULL,
  `update_date` DATE NULL,
  PRIMARY KEY (`linksocial_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `learnit`.`discount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `learnit`.`discount` (
  `courses_id` INT NOT NULL,
  `percent` DOUBLE NULL,
  `description` NVARCHAR(1500) NULL,
  `start_date` DATE NULL,
  `end_date` DATE NULL,
  PRIMARY KEY (`courses_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
