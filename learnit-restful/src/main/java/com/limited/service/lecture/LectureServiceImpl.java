package com.limited.service.lecture;


import com.limited.entity.LectureEntity;
import com.limited.repository.lecture.LectureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 4/25/2016.
 */
@Service
@Transactional
public class LectureServiceImpl implements LectureService {

    @Autowired
    LectureRepository lectureRepository;

    @Override
    public LectureEntity create(LectureEntity lectureEntity) {

        LectureEntity temp = lectureRepository.save(lectureEntity);

        return temp;
    }
}
