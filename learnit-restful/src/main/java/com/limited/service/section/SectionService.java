package com.limited.service.section;

import com.limited.entity.SectionEntity;


import java.util.List;

/**
 * Created by Administrator on 4/14/2016.
 */
public interface SectionService {

    SectionEntity create(SectionEntity sectionEntity);

    void delete(int id);

    List<SectionEntity> getListSection(int id);


    SectionEntity getSection(int id);

    void update(int id, String title, String goalsSection);
}
