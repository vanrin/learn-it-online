package com.limited.service.section;

import com.limited.entity.SectionEntity;
import com.limited.repository.lecture.LectureRepository;
import com.limited.repository.section.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 4/14/2016.
 */
@Service
@Transactional
public class SectionServiceImpl implements SectionService {

    @Autowired
    SectionRepository sectionRepository;
    @Autowired
    LectureRepository lectureRepository;

    @Override
    public SectionEntity create(SectionEntity sectionEntity) {

        SectionEntity entity = sectionRepository.save(sectionEntity);
        return entity;
    }

    @Override
    public void delete(int id) {

        sectionRepository.delete(id);

    }

    @Override
    public List<SectionEntity> getListSection(int id) {

        return sectionRepository.findAllSectionByCourseId(id);
    }

    @Override
    public SectionEntity getSection(int id) {
        return sectionRepository.findOne(id);
    }

    @Override
    public void update(int id, String title, String goalsSection) {

        sectionRepository.updateSection(id, title, goalsSection);

    }


}
