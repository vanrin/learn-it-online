package com.limited.service.user;

import com.limited.entity.UserEntity;

public interface UserService {

    UserEntity create(UserEntity userEntity);

    UserEntity login(UserEntity userEntity);

    UserEntity registerViaFacebook(UserEntity userEntity);
}
