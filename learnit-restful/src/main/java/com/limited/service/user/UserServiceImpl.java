package com.limited.service.user;

import com.limited.entity.UserEntity;
import com.limited.exception.ValidateException;
import com.limited.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserEntity create(UserEntity userEntity) {

        if (userEntity != null) {
            //find user have email exist database
            UserEntity userCheck = userRepository.findById(userEntity.getId());
            // if not user in database ,
            if (userCheck == null) {
                // Insert user in database
                UserEntity another = userRepository.save(userEntity);
                return another;
            } else {
                // return null when user had email in database
                return null;
            }
        } else {
            throw new ValidateException("Invalid data");
        }
    }


    @Override
    public UserEntity login(UserEntity userEntity) {

        UserEntity userEntityTemp = userRepository.findByIdAndPassword(userEntity.getId(), userEntity.getPassword());

        return userEntityTemp;
    }

    @Override
    public UserEntity registerViaFacebook(UserEntity userEntity) {

        if (userEntity != null) {
            //find user have facebook id  exist database
            UserEntity userCheck = userRepository.findById(userEntity.getId());

            // if not user in database ,
            if (userCheck == null) {
                // Insert user in database
                UserEntity another = userRepository.save(userEntity);
                return another;
            } else {
                // return null when user had email in database
                return null;
            }
        } else {
            throw new ValidateException("Invalid data");
        }
    }
}
