package com.limited.service.course;

import com.limited.entity.CourseEntity;

import java.util.List;

/**
 * Created by Administrator on 4/25/2016.
 */
public interface CourseService {

    CourseEntity createCourse(CourseEntity courseEntity);

    void createGoalsCourse(CourseEntity courseEntity);

    List<CourseEntity> getListCourse();
}
