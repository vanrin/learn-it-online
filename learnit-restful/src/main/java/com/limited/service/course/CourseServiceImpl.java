package com.limited.service.course;

import com.limited.entity.CourseEntity;
import com.limited.exception.ValidateException;
import com.limited.repository.course.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 4/25/2016.
 */
@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Override
    public CourseEntity createCourse(CourseEntity courseEntity) {

        if (courseEntity != null) {
            CourseEntity courseCheck = courseRepository.findOne(courseEntity.getId());
            if (courseCheck == null) {
                CourseEntity another = courseRepository.save(courseEntity);
                return another;
            } else {
                return null;
            }

        } else {
            throw new ValidateException("Invalid data");
        }

    }

    @Override
    public void createGoalsCourse(CourseEntity courseEntity) {

        courseRepository.updateGoalsCourse(courseEntity.getGoalsCourse(), courseEntity.getTargetAudience(), courseEntity.getRequirement(), courseEntity.getId());

    }

    @Override
    public List<CourseEntity> getListCourse() {

        return courseRepository.findAll();
    }

}