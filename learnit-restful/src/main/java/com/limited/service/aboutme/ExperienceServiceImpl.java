package com.limited.service.aboutme;

import com.limited.entity.ExperienceEntity;
import com.limited.repository.aboutme.ExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 5/13/2016.
 */
@Service
public class ExperienceServiceImpl implements ExperienceService {

    @Autowired
    ExperienceRepository experienceRepository;

    @Override
    public ExperienceEntity createExperience(ExperienceEntity experienceEntity) {

        ExperienceEntity entity = experienceRepository.save(experienceEntity);
        return entity;
    }
}
