package com.limited.service.aboutme;

import com.limited.entity.CertificationEntity;
import com.limited.repository.aboutme.CertificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 5/13/2016.
 */
@Service
public class CertificationServiceImpl implements CertificationService {

    @Autowired
    CertificationRepository certificationRepository;

    @Override
    public CertificationEntity createCertification(CertificationEntity certificationEntity) {

        CertificationEntity entity = certificationRepository.save(certificationEntity);
        return entity;
    }
}
