package com.limited.service.aboutme;

import com.limited.entity.CertificationEntity;

/**
 * Created by Administrator on 5/13/2016.
 */
public interface CertificationService {

    CertificationEntity createCertification(CertificationEntity certificationEntity);
}
