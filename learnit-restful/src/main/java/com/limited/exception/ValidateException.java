package com.limited.exception;

/**
 * Created by Administrator on 4/7/2016.
 */
public class ValidateException extends RuntimeException{
    
    public ValidateException() {
        super();
    }

    public ValidateException(String error) {
        super(error);
    }

}
