package com.limited.exception;

/**
 * Created by Administrator on 4/7/2016.
 */
public class ResourceNotFound extends RuntimeException{

    public ResourceNotFound() {
        super();
    }

    public ResourceNotFound(String error) {
        super(error);
    }


}
