package com.limited.controller.course;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.constant.Constant;
import com.limited.entity.CourseEntity;
import com.limited.service.course.CourseService;
import com.limited.template.ResponseTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 4/25/2016.
 */
@RestController
@RequestMapping(value = "/course")
public class CourseController {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    CourseService courseService;

    @RequestMapping(value = "/addCourse")
    public ResponseTemplate addCourse(@RequestBody String dataCourse) throws IOException {

        CourseEntity courseEntity = objectMapper.readValue(dataCourse, CourseEntity.class);
        CourseEntity anotherCourse = courseService.createCourse(courseEntity);
        if (anotherCourse != null) {
            return new ResponseTemplate(Constant.GET_API_SUCCESS, anotherCourse, "Insert title ok");

        } else {
            return new ResponseTemplate(Constant.GET_API_FAIL, null, "Create section fail.");
        }

    }

    @RequestMapping(value = "/goalsCourse")
    public Object addGoalsCourse(@RequestBody String stringGoalsCourse) throws IOException {

        try {
            CourseEntity courseEntity = objectMapper.readValue(stringGoalsCourse, CourseEntity.class);
            courseService.createGoalsCourse(courseEntity);

            return new HashMap<String, String>() {
                {
                    put("result", "success");
                }
            };

        } catch (Exception e) {

            return new HashMap<String, String>() {
                {
                    put("result", "fail");
                }
            };
        }


    }

    @RequestMapping(value = "/listCourse", method = RequestMethod.GET)
    public Object getListCourse() {

        List<CourseEntity> listCourseEntity = courseService.getListCourse();
        return listCourseEntity;
    }
}
