package com.limited.controller.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.constant.Constant;
import com.limited.entity.UserEntity;
import com.limited.service.user.UserService;
import com.limited.template.ResponseTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


/**
 * Created by Administrator on 4/7/2016.
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseTemplate addUser(@RequestBody String userData) {
        try {
            // read json web send and convert to object
            UserEntity userEntity = objectMapper.readValue(userData, UserEntity.class);
            UserEntity anotherUser = userService.create(userEntity);
            if (anotherUser != null) {
                return new ResponseTemplate(Constant.GET_API_SUCCESS, anotherUser, "Create user successfully.");
            } else {
                return new ResponseTemplate(Constant.GET_API_FAIL, null, "Create user fail.");
            }
        } catch (IOException e) {
            return new ResponseTemplate(Constant.GET_API_FAIL, null, "Create user fail.");
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public UserEntity login(@RequestBody String userData) {
        try {
            // read json web send and convert to object
            UserEntity userEntity = objectMapper.readValue(userData, UserEntity.class);
            if (userEntity != null) {
                UserEntity anotherUser = userService.login(userEntity);
                if (anotherUser != null) {
                    return anotherUser;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (IOException e) {
            return null;
        }
    }

    @RequestMapping(value = "/registerVFacebook", method = RequestMethod.POST)
    public ResponseTemplate registerFacebook(@RequestBody String userData) {
        try {
            // read json web send and convert to object
            UserEntity userEntity = objectMapper.readValue(userData, UserEntity.class);

            UserEntity anotherUser = userService.registerViaFacebook(userEntity);
            if (anotherUser != null) {
                return new ResponseTemplate(Constant.GET_API_SUCCESS, anotherUser, "Register user successfully.");
            } else {
                return new ResponseTemplate(Constant.GET_API_FAIL, null, "Register user fail.");
            }
        } catch (IOException e) {
            return new ResponseTemplate(Constant.GET_API_FAIL, null, "Register user fail.");
        }

    }

}

