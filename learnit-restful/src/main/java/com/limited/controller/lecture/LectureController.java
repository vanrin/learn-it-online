package com.limited.controller.lecture;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.constant.Constant;
import com.limited.entity.LectureEntity;
import com.limited.service.lecture.LectureService;
import com.limited.template.ResponseTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Administrator on 4/25/2016.
 */

@RestController
@RequestMapping(value = "/lecture")
public class LectureController {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    LectureService lectureService;

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public ResponseTemplate handleFileUpload(@RequestParam("file") MultipartFile[] files) {
        int count = 0;
        if (files != null && files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                try {
                    byte[] bytes = files[i].getBytes();
                    // create file on server
                    BufferedOutputStream stream =
                            new BufferedOutputStream(new FileOutputStream(new File("E:\\RESULT\\" + files[i].getOriginalFilename())));
                    stream.write(bytes);
                    stream.close();
                    count++;
                } catch (Exception e) {
                    e.printStackTrace();
                    return new ResponseTemplate(Constant.GET_API_FAIL, null, "You successfully fail ");
                }
            }
            if (count == files.length) {
                return new ResponseTemplate(Constant.GET_API_SUCCESS, null, "Upload file success");
            } else {
                return new ResponseTemplate(Constant.GET_API_FAIL, null, "You successfully fail");
            }
        } else {
            return new ResponseTemplate(Constant.GET_API_FAIL, null, "You successfully fail");
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public LectureEntity addLecture(@RequestBody String lectureData) {

        try {
            LectureEntity lectureEntity = objectMapper.readValue(lectureData, LectureEntity.class);
            LectureEntity another = lectureService.create(lectureEntity);
            return another;
        } catch (Exception e) {
            return null;
        }
    }

}
