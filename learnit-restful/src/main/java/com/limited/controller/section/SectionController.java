package com.limited.controller.section;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.constant.Constant;
import com.limited.entity.SectionEntity;
import com.limited.service.section.SectionService;
import com.limited.template.ResponseTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 4/14/2016.
 */
@RestController
@RequestMapping(value = "/section")
public class SectionController {

    @Autowired
    SectionService sectionService;
    @Autowired
    ObjectMapper objectMapper;

    @RequestMapping(value = "/getSection", method = RequestMethod.POST)
    public ResponseTemplate getSection(@RequestBody String sectionData) throws IOException {

        SectionEntity sectionEntity = objectMapper.readValue(sectionData, SectionEntity.class);
        SectionEntity entity = sectionService.getSection(sectionEntity.getId());
        if (entity != null) {
            return new ResponseTemplate(Constant.GET_API_SUCCESS, entity, "find ok ");
        } else {
            return new ResponseTemplate(Constant.GET_API_FAIL, null, "not find");

        }

    }

    @RequestMapping(value = "/deleteSection/{id}", method = RequestMethod.DELETE)
    public ResponseTemplate deleteBook(@PathVariable("id") int sectionId) {

        sectionService.delete(sectionId);
        return new ResponseTemplate(Constant.GET_API_SUCCESS, null, "Ok");
    }

    @RequestMapping(value = "/listSection/{courseID}", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getListSection(@PathVariable("courseID") int idCourse) {

        List<SectionEntity> listData = sectionService.getListSection(idCourse);
        List<Map<String, Object>> result = new ArrayList<>();
        if (listData != null) {
            for (SectionEntity sectionEntity : listData) {
                Map<String, Object> map = new HashMap<>();
                map.put("id", sectionEntity.getId());
                map.put("title", sectionEntity.getTitle());
                map.put("goalsSection", sectionEntity.getGoalsSection());
                map.put("description", sectionEntity.getDescription());
                result.add(map);
            }
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST)
    public SectionEntity addSection(@RequestBody String sectionData) {

        try {
            SectionEntity sectionEntity = objectMapper.readValue(sectionData, SectionEntity.class);
            SectionEntity another = sectionService.create(sectionEntity);
            return another;
        } catch (Exception e) {
            return null;
        }
    }

    @RequestMapping(value = "/updateSection", method = RequestMethod.PUT)
    public Object updateSection(@RequestBody String sectionData) throws IOException {

        try {
            SectionEntity sectionEntity = objectMapper.readValue(sectionData, SectionEntity.class);
            sectionService.update(sectionEntity.getId(), sectionEntity.getTitle(), sectionEntity.getGoalsSection());
            return new HashMap<String, String>() {
                {
                    put("result", "success");
                }
            };

        } catch (Exception e) {
            return new HashMap<String, String>() {
                {
                    put("result", "fail");
                }
            };
        }

    }
}
