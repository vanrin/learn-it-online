package com.limited.controller.aboutme;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.entity.CertificationEntity;
import com.limited.service.aboutme.CertificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Administrator on 5/13/2016.
 */
@RestController
@RequestMapping(value = "/certification")
public class CertificationController {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CertificationService certificationService;

    @RequestMapping(value = "/addCertification")
    public Object addCertification(@RequestBody String certificationEntity) throws IOException {

        CertificationEntity entity = objectMapper.readValue(certificationEntity, CertificationEntity.class);
        CertificationEntity another = certificationService.createCertification(entity);
        return another;
    }
}
