package com.limited.controller.aboutme;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.entity.ExperienceEntity;
import com.limited.service.aboutme.ExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Administrator on 5/13/2016.
 */
@RestController
@RequestMapping(value = "/experience")
public class ExperienceController {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ExperienceService experienceService;


    @RequestMapping(value = "/addExperience")
    public Object addCertification(@RequestBody String experienceEntity) throws IOException {

        ExperienceEntity entity = objectMapper.readValue(experienceEntity, ExperienceEntity.class);
        ExperienceEntity another = experienceService.createExperience(entity);
        return another;
    }

}
