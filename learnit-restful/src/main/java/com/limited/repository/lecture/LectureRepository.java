package com.limited.repository.lecture;

import com.limited.entity.LectureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 4/28/2016.
 */
public interface LectureRepository extends JpaRepository<LectureEntity, Integer> {
}
