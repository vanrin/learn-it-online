package com.limited.repository.user;

import com.limited.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 4/7/2016.
 */

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    @Transactional
    @Query("SELECT u FROM UserEntity u WHERE u.id = ?1 AND u.password = ?2")
    UserEntity findByIdAndPassword(String id, String password);


    UserEntity findById(String id);
}
