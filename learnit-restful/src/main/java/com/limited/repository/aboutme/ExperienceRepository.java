package com.limited.repository.aboutme;

import com.limited.entity.ExperienceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 5/13/2016.
 */
@Repository
public interface ExperienceRepository extends JpaRepository<ExperienceEntity, Integer> {
}
