package com.limited.repository.aboutme;

import com.limited.entity.CertificationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 5/13/2016.
 */
@Repository
public interface CertificationRepository extends JpaRepository<CertificationEntity, Integer> {

}
