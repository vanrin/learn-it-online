package com.limited.repository.course;

import com.limited.entity.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by Administrator on 4/25/2016.
 */
@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, Integer> {

    @Modifying
    @Transactional
    @Query("UPDATE CourseEntity c SET c.goalsCourse = ?1, c.targetAudience = ?2, c.requirement= ?3 WHERE c.id = ?4")
    void updateGoalsCourse(String goalsCourse, String targetAudience, String requirement, int id);

}
