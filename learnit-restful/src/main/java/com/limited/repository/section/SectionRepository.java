package com.limited.repository.section;


import com.limited.entity.SectionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


/**
 * Created by Administrator on 4/14/2016.
 */
@Repository
public interface SectionRepository extends JpaRepository<SectionEntity, Integer> {


    SectionEntity findById(int id);

//    @Modifying
//    @Query("UPDATE SectionEntity s set s.title = ?2, s.goalsSection = ?3 WHERE c.id = ?1")
//    void updateSection(int id, String title, String goalsSection);


    @Modifying
    @Transactional
    @Query("UPDATE SectionEntity s SET s.title = ?2, s.goalsSection = ?3 WHERE s.id = ?1")
    void updateSection(int id, String title, String goalsSection);

    @Query("SELECT s FROM SectionEntity s WHERE s.courseEntity.id= ?1")
    List<SectionEntity> findAllSectionByCourseId(int id);
}
