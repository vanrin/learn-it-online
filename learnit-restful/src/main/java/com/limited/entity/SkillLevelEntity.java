package com.limited.entity;

import javax.persistence.*;

/**
 * Created by Administrator on 5/13/2016.
 */
@Entity
@Table(name = "language")
public class SkillLevelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "level_id")
    private int id;
    @Column(name = "level_value")
    private String levelValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevelValue() {
        return levelValue;
    }

    public void setLevelValue(String levelValue) {
        this.levelValue = levelValue;
    }
}
