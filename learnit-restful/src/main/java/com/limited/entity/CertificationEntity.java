package com.limited.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 5/10/2016.
 */
@Entity
@Table(name = "certification")
public class CertificationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "certification_id")
    private int id;
    private String title;
    private String center;
    @Column(name = "valid_from")
    private String validFrom;
    @Column(name = "valid_to")
    private String validTo;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private UserEntity userEntityC;

    public CertificationEntity() {

    }

    public UserEntity getUserEntityC() {
        return userEntityC;
    }

    public void setUserEntityC(UserEntity userEntityC) {
        this.userEntityC = userEntityC;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
