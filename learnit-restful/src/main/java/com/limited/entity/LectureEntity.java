package com.limited.entity;

import javax.persistence.*;

/**
 * Created by Administrator on 4/25/2016.
 */
@Entity
@Table(name = "lecture")
public class LectureEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lecture_id")
    private int id;
    private String title;
    @Column(name = "duration_video")
    private String durationVideo;
    @Column(name = "link_video")
    private String linkVideo;
    @Column(name = "source_code")
    private String sourceCode;
    private String description;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "section_id")
    private  SectionEntity sectionEntity;



    public LectureEntity() {

    }

    public SectionEntity getSectionEntity() {
        return sectionEntity;
    }

    public void setSectionEntity(SectionEntity sectionEntity) {
        this.sectionEntity = sectionEntity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDurationVideo() {
        return durationVideo;
    }

    public void setDurationVideo(String durationVideo) {
        this.durationVideo = durationVideo;
    }

    public String getLinkVideo() {
        return linkVideo;
    }

    public void setLinkVideo(String linkVideo) {
        this.linkVideo = linkVideo;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
