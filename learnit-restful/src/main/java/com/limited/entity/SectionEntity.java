package com.limited.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 4/14/2016.
 */
@Entity
@Table(name = "section")
public class SectionEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "section_id")
    private int id;
    private String title;
    @Column(name = "goals_section")
    private String goalsSection;
    private  String description;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sectionEntity")
    private List<LectureEntity> lectureEntities = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "course_id")
    private CourseEntity courseEntity;

    public SectionEntity() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CourseEntity getCourseEntity() {
        return courseEntity;
    }

    public void setCourseEntity(CourseEntity courseEntity) {
        this.courseEntity = courseEntity;
    }



    public List<LectureEntity> getLectureEntities() {
        return lectureEntities;
    }

    public void setLectureEntities(List<LectureEntity> lectureEntities) {
        this.lectureEntities = lectureEntities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGoalsSection() {
        return goalsSection;
    }

    public void setGoalsSection(String goalsSection) {
        this.goalsSection = goalsSection;
    }


}
