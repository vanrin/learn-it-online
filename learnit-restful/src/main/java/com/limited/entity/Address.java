package com.limited.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by Administrator on 4/5/2016.
 */
@Embeddable
public class Address {

    private String street;
    private String city;
    private String province;
    private String country;
    @Column(name = "post_code")
    private String postCode;

    public Address() {

    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
