package com.limited.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 4/19/2016.
 */
@Entity
@Table(name = "course")
public class CourseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "courses_id")
    private int id;
    private String image;
    private String title;
    private String description;
    private String requirement;
    @Column(name = "goals_course")
    private String goalsCourse;
    @Column(name = "target_audience")
    private String targetAudience;
    @Column(name = "cost_course")
    private double costCourse;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "courseEntity")
    private List<SectionEntity> sectionEntities = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private UserEntity userEntityCo;

    public CourseEntity() {

    }

    public UserEntity getUserEntityCo() {
        return userEntityCo;
    }

    public void setUserEntityCo(UserEntity userEntityCo) {
        this.userEntityCo = userEntityCo;
    }


    public List<SectionEntity> getSectionEntities() {
        return sectionEntities;
    }

    public void setSectionEntities(List<SectionEntity> sectionEntities) {
        this.sectionEntities = sectionEntities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getGoalsCourse() {
        return goalsCourse;
    }

    public void setGoalsCourse(String goalsCourse) {
        this.goalsCourse = goalsCourse;
    }

    public String getTargetAudience() {
        return targetAudience;
    }

    public void setTargetAudience(String targetAudience) {
        this.targetAudience = targetAudience;
    }

    public double getCostCourse() {
        return costCourse;
    }

    public void setCostCourse(double costCourse) {
        this.costCourse = costCourse;
    }


}
