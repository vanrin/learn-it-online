package com.limited.entity;

import javax.persistence.*;

/**
 * Created by Administrator on 4/5/2016.
 */
@Entity
@Table(name = "linksocial")
public class LinkSocial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "linksocial_id")
    private long id;
    @Column(name = "social_value")
    private String socialValue;
    @Column(name = "create_date")
    private String createDate;

    public LinkSocial() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSocialValue() {
        return socialValue;
    }

    public void setSocialValue(String socialValue) {
        this.socialValue = socialValue;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
