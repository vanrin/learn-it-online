<div class="col-sm-2 left-bar">
    <h3></h3>
    <ul class="list-group">
        <li class="list-group-item cc"><span>COURSE CONTENT</span></li>
        <li class="list-group-item"><a href="/course/goalsCourse/${courseID}">Course Goals</a></li>
        <li class="list-group-item"><a href="/section/homeSection/${courseID}">Section</a></li>
        <li class="list-group-item"><a href="/lecture/createLecture/${courseID}">Lecture</a></li>
        <%--<li class="list-group-item"><a href="/lecture/createLecture">Lecture</a></li>--%>
        <li class="list-group-item"><a href="/">Courses Feedback</a></li>
        <li class="list-group-item cc"><span>COURSE INFO</span></li>
        <li class="list-group-item"><a href="/course/basicCourse/${courseID}">Basics</a></li>
        <li class="list-group-item"><a href="/">Courses Summary</a></li>
        <li class="list-group-item"><a href="/">Image</a></li>
        <li class="list-group-item"><a href="/">Automatic Messages</a></li>
        <li class="list-group-item cc"><span>COURSE TEST</span></li>
        <li class="list-group-item"><a href="/">Final Test</a></li>
        <li class="list-group-item cc"><span>ABOUT ME</span></li>
        <li class="list-group-item"><a href="/experience/addExperience">Experience</a></li>
        <li class="list-group-item"><a href="/certification/addCertification">Certification</a></li>
    </ul>

</div>
