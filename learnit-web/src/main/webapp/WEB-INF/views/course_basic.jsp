<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/css/maincssgui.css" rel="stylesheet">
</head>
<body>

<jsp:include page="navCourses.jsp"/>
<div class="container">

    <jsp:include page="headerCourses.jsp"/>


    <div class="row">
        <jsp:include page="menuCourses.jsp"/>

        <div class="col-sm-8 centerView">
            <div class="addLecture">
                <div class="col-sm-8 formAddLecture">

                    <div class="titleCourse">
                        <label>Title :</label>
                        <input   type="text" class="form-control title"
                                 placeholder="Enter a title" required="required"/> <br>
                    </div>

                    <div class="languageCourse">
                        <label class="col-xs-3 control-label">Language :</label>
                        <div class="col-xs-12 selectContainer">
                            <select class="form-control" name="size">
                                <option value="">Choose a size</option>
                                <option value="s">Small (S)</option>
                                <option value="m">Medium (M)</option>
                                <option value="l">Large (L)</option>
                                <option value="xl">Extra large (XL)</option>
                            </select>
                        </div>
                    </div>
                    <div class="levelCourse">
                        <h2>Instructional Level</h2>
                        <p>Specify the level of the course content to give students a better understanding of whether this course is right for them.</p>
                        <form role="form">
                            <label class="radio-inline">
                                <input type="radio" name="optradio">Beginner Level
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optradio">Intermediate Level
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optradio">Expert Level
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optradio">All Levels
                            </label>
                        </form>
                    </div>

                    <div class="col-sm-8 " style="float: right;">
                        <input type="submit" style="margin-top: 20px;"  class="btn btn-success" name="" value="SAVE" placeholder="">
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


<script src="jquery/mainj.js" type="text/javascript" ></script>
</body>
</html>