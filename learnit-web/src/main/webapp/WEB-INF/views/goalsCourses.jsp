<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>goals course</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/css/maincssgui.css" rel="stylesheet">
</head>
<body>

   <jsp:include page="navCourses.jsp"/>
<div class="container">

    <jsp:include page="headerCourses.jsp"/>


    <div class="row">
        <jsp:include page="menuCourses.jsp"/>

        <div class="col-sm-8 centerView">
            <form:form id="signupForm" action="../doAddGoalsCourse" method="post" modelAttribute="courseEntity">
            <div class="goalsCourse">
                <div class="">

                    <form:input  path="id"  type="hidden"
                                 disabled="disabled"  /> <br>
                </div>
                <div class="col-sm-12">
                    <h5>At the end of my course, students will be able to...	</h5>
                    <h6>Start with a verb. Include details on specific skills students<br> will learn and where students can apply them.</h6>
                </div>

                <div class="col-sm-8">
                   <form:input path="goalsCourse" type="text" class="form-control txC" name="" value="" placeholder=""/>
                </div>

                <div class="col-sm-3">
                    <input type="submit"  class="btn btn-success" name="" value="Add" placeholder="">
                </div>
            </div>

            <div class="goalsCourse" style="padding-top: 130px;">
                <div class="col-sm-12">
                    <h5>Who should take this course? Who should not?	</h5>
                    <h6>Students appreciate instructors who set the right expectations by telling them what specific <br> student needs they are solving, who the course is best suited for, and who it is NOT for.</h6>
                </div>

                <div class="col-sm-8" id="them">
                    <form:input path="targetAudience" type="text" class="form-control txC" name="" value="" placeholder=""/>
                </div>

                <div class="col-sm-3">
                    <input type="submit" id="add" class="btn btn-success" name="" value="Add" placeholder="">
                </div>

            </div>
            <div class="goalsCourse" style="padding-top: 90px;">
                <div class="col-sm-12">
                    <h5>What will students need to know or do before starting this course?</h5>
                    <h6>What materials/software do the students need and what actions do they have to perform<br> before the course begins?</h6>
                </div>

                <div class="col-sm-8">
                    <form:input path="requirement" type="text" class="form-control txC" name="" value="" placeholder=""/>
                </div>

                <div class="col-sm-3">
                    <input type="submit"  class="btn btn-success" name="" value="Add" placeholder="">
                </div>
            </div>
            <div style="clear: both;">
            </div>

            <div class="col-sm-8 " style="float: right;">
                <input type="submit" style="margin-top: 40px;"  class="btn btn-success" name="" value="SAVE" placeholder="">
            </div>

            </form:form>
        </div>
    </div>
</div>



</body>
</html>