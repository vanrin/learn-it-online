<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>IT</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../../legend/css/bootstrap.css" rel="stylesheet">
    <link href="../../legend/css/style.css" rel="stylesheet">
    <link rel='stylesheet' id='prettyphoto-css'  href="../../legend/css/prettyPhoto.css" type='text/css' media='all'>
    <link href="../../legend/css/fontello.css" type="text/css" rel="stylesheet">
    <!--[if lt IE 7]>
    <link href="css/fontello-ie7.css" type="text/css" rel="stylesheet">
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <style type="text/css">
        body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
        }
    </style>
    <link href="../../legend/css/bootstrap-responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../../legend/js/html5.js"></script>
    <![endif]-->
    <script src="../../legend/js/jquery.js"></script>
    <script src="../../legend/js/jquery.scrollTo-1.4.2-min.js"></script>
    <script src="../../legend/js/jquery.localscroll-1.2.7-min.js"></script>
    <script charset="utf-8">
        $(document).ready(function () {
            $("a[rel^='prettyPhoto']").prettyPhoto();
        });
    </script>
</head>
<body>

<div id="top"></div>
<div id="headerwrap">
    <header class="clearfix">
        <h1><span>Ready to Create a Course?</span><br> Start by entering the title of a course:</h1>
        <div class="container">
            <div class="row">
                <div class="span12">
                    <form:form id="signupForm" action="/course/doAddCourse" method="post" modelAttribute="course">

                        <form:input type="text" path="title" name="your-email" placeholder="e.g. Learn Androi" class="cform-text" size="40"  required="required"/>
                        <input type="submit" value="Create Course" class="cform-submit"/>
                    </form:form>
                </div>
            </div>
            <div class="row">
                <div class="span12">

                </div>
            </div>
        </div>
    </header>
</div>
<script src="../../legend/js/bootstrap.js"></script>
<script src="../../legend/js/jquery.prettyPhoto.js"></script>
<script src="../../legend/js/site.js"></script>
</body>
</html>