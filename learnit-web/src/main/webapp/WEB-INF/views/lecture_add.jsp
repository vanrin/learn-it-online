<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/css/maincssgui.css" rel="stylesheet">
</head>
<body>

<jsp:include page="navCourses.jsp"/>
<div class="container">
    <jsp:include page="headerCourses.jsp"/>
    <div class="row">
        <jsp:include page="menuCourses.jsp"/>

        <div class="col-sm-8 centerView">
            <div class="addLecture">
                <div class="col-sm-8 formAddLecture">

                    <form:form id="signupForm" action="../../lecture/doAddLecture" method="post"
                               enctype="multipart/form-data" modelAttribute="lecture">


                       <div>
                            <td><form:select path="sectionEntity.id" class="form-control " items="${listSection}"
                                             itemValue="id" itemLabel="title"/></td>
                        </div>

                        <br>

                        <div class="">
                            <form:input path="title" id="title" type="text" placeholder="Enter a Title"
                                        class="form-control "
                                        required="required"/> <br>
                        </div>

                        <div class="">
                            <form:input path="durationVideo" id="durationVideo" placeholder="Enter duration video"
                                        type="text" class="form-control "
                                        required="required"/> <br>
                        </div>

                        <span style="color: #0088cc">Upload Video</span>
                        <div class="">
                            <input value="Choose Video" type="file" name="file"
                                        class="btn btn-info"
                                    /> <br>
                        </div>
                        <span style="color: #0088cc">Upload Source</span>

                        <div class="sourceCode">
                            <input  value="Choose Source" type="file" name="file"
                                        class="btn btn-info"
                                    /> <br>
                        </div>
                        <span style="color: #0088cc">Description</span>

                        <div class="description">
                            <form:textarea class="form-control" path="description" cols="70" rows="5" required="required"/>
                            <br>
                        </div>

                        <div class="col-sm-8 " style="float: right;">
                            <input type="submit" style="margin-top: 20px;" class="btn btn-success" name="" value="SAVE"
                                   placeholder="">
                        </div>

                    </form:form>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="jquery/mainj.js" type="text/javascript"></script>
</body>
</html>