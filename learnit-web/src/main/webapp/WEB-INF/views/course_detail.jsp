<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html" charset="utf-8"/>

    <script type="text/javascript" src="../../resources/singlepage/js/jquery-2.0.2.min.js"></script>
    <link href="../../resources/singlepage/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<jsp:include page="navCourses.jsp"/>

<div id="container">
    <div id="main-content">

        <div class="content-wrap">
            <div class="content">
                <h1 class="single-post-title">Running a Mobile App Dev Business: The Complete Guide</h1>
                <p>Learn how to start and grow a mobile app development business. Get up & running and making money in less than 1 week.</p>

                <video width="610" height="340" controls >
                    <source src="media/androi.mp4"  type="video/mp4" media="">

                </video>
                <p>
                    "In the future, most people will access the internet through a mobile device. Mobile is the future." - Eric Schmidt, Chairman of Google
                </p>
                <p>
                    Today, companies and small businesses are increasingly focusing on building a mobile app presence. That represents a huge opportunity for you - bigger than it's ever been before.
                </p>
                <p>
                    The mobile app development industry is growing at a blazing 43% per year and shows no signs of slowing down.
                </p>
                <h4>What are the requirements?</h4>
                <p>A computer with OSX installed.
                    Basic Javascript knowledge. You will learn the fundamentals of React in this course, no prior knowledge required</p>
                <h4>What am I going to get from this course?</h4>
                <p>Over 72 lectures and 8 hours of content!
                    Build mobile apps for iOS using React Native
                    Prototype and deploy your own applications to the Apple Store
                    Get up to speed with React design principles and methodologies
                    Learn the differences between using React in a browser and on a mobile device
                    Discover mobile design patterns used by experienced engineers</p>
                <h4>What is the target audience?</h4>
                <p>Anyone interested in building mobile applications much faster than using Swift or Java
                </p>
                <h4>Section 1: Start Here!</h4>
                    <p>Lecture 1 :Introduction

                    </p>
                <p>Lecture 2 :Github Repository & What to do when you are stuck
                </p>
                <h4>Section 2: Start Here!</h4>
                <p>Lecture 1 :Introduction

                </p>
                <p>Lecture 2 :Github Repository & What to do when you are stuck
                </p>
                <h4>Section 3: Start Here!</h4>
                <p>Lecture 1 :Introduction

                </p>
                <p>Lecture 2 :Github Repository & What to do when you are stuck
                </p>

            </div>
        </div><!--end .content-->

    </div><!--end #main-content-->

    <div id="sidebar">

        <div class="sidebar-block" id="fb-block">
            <div class="sidebar-block-title">
                <span>Connect Facebook</span>
            </div>
            <div class="sidebar-block-content">


            </div>
        </div>

        <div class="sidebar-block">
            <div class="sidebar-block-title">
                <span>COURSE</span>
            </div>
            <div class="sidebar-block-content">
                <ul>
                    <li><a href="#">Lecture : </a></li>
                    <li><a href="#">Video : </a></li>
                    <li><a href="#">Skill Level : </a></li>
                    <li><a href="#">Languages</a></li>

                </ul>
            </div>
        </div>

        <div class="sidebar-block" id="other-post">
            <div class="sidebar-block-title">
                <span>Có thể bạn quan tâm</span>
            </div>
            <div class="sidebar-block-content">
                <div class="other-post-item">
                    <div class="other-post-img">
                        <a href="#"><img src="../../resources/singlepage/images/post1.jpg"></a>
                    </div>
                    <div class="other-post-info">
                        <a href="#">
                            Build Apps with React Native</a>
                        <p>
                            Paphiopedilum delenatii</p>
                    </div>
                </div>

                <div class="other-post-item">
                    <div class="other-post-img">
                        <a href="#"><img src="../../resources/singlepage/images/post2.jpg"></a>
                    </div>
                    <div class="other-post-info">
                        <a href="#">Build Apps with React Native</a>
                        <p>Rhopalocnemis phalloides...</p>
                    </div>
                </div>

                <div class="other-post-item">
                    <div class="other-post-img">
                        <a href="#"><img src="../../resources/singlepage/images/post3.jpg"></a>
                    </div>
                    <div class="other-post-info">
                        <a href="#">Build Apps with React Native</a>
                        <p>
                            Melanorrhea laccifera...</p>
                    </div>
                </div>

                <div class="other-post-item">
                    <div class="other-post-img">
                        <a href="#"><img src="../../resources/singlepage/images/post4.jpg"></a>
                    </div>
                    <div class="other-post-info">
                        <a href="#">Build Apps with React Native</a>
                        <p>
                            Parashorea stellata...</p>
                    </div>
                </div>

                <div class="other-post-item">
                    <div class="other-post-img">
                        <a href="#"><img src="../../resources/singlepage/images/post5.jpg"></a>
                    </div>
                    <div class="other-post-info">
                        <a href="#">
                            Build Apps with React Native</a>
                        <p>
                            Thrixspermum sutepense...</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!--end #sidebar-->
</div><!--end #container-->

</body>
</html>