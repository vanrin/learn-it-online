<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html" charset="utf-8"/>

    <script type="text/javascript" src="../../resources/singlepage/js/jquery-2.0.2.min.js"></script>
    <link href="../../resources/singlepage/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="header-wrap">
    <div id="header">
        <div id="main-nav">
            <ul>
                <li><a href="/">HOME</a></li>
                <li><a href="/logout/logoutAccount">LOGOUT</a></li>
                <%--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Browse Courses <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                    </ul>
                </li>--%>

            </ul>
        </div>	<!--end #main-nav-->

        <%--<div id="search-box">
            <form>
                <input id="txt-search" type="text" name="q" value="" placeholder="...">
                <input id="btn-search" type="submit" name="search" value="SEARCH">
            </form>
        </div>--%>

    </div>
</div><!--end #header-wrap-->