<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--Load jQuery and the validate plugin--%>
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
    (function ($, W, D) {
        var JQUERY4U = {};
        JQUERY4U.UTIL =
        {
            setupFormValidation: function () {
                //form validation rules
                $("#signupForm").validate({
                    rules: {
                        firstName: "required",
                        lastName: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            /* minlength: 5*/
                        },
                        agree: "required"
                    },
                    messages: {
                        firstName: "Please enter your first name",
                        lastName: "Please enter your last name",
                        password: {
                            required: "Please provide a password",
                            /* minlength: "Your password must be at least 5 characters long"*/
                        },
                        email: "Please enter a valid email address",

                    },
                    submitHandler: function (form) {
                        form.submit();
                    }
                });
            }
        };

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);

</script>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Register page based on Bootstrap 3.3.6">
    <meta name="author" content="ksud">
    <title>|Sign-up|</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/bootstrap/bootstrap-3.3.6/css/bootstrap.css">
    <!-- Custom styles for this template -->
    <link href="/bootstrap/css/custom.css" rel="stylesheet">
    <link href="/bootstrap/css/theme.css" rel="stylesheet">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <div class="formRegister" style="border:1px;">
        <div class="panel panel-default ">
            <div class="form-signin">
                <h3 class="form-signin-heading text-center">Sign up </h3>




                <div class="row">
                    <form:form id="signupForm" action="doAddUser" method="post" modelAttribute="user">


                        <label class="sr-only">First Name : </label>

                        <form:input path="firstName" id="firstName" type="text" class="form-control"
                                    placeholder="FirstName"/>

                        <label class="sr-only">Last Name : </label>
                        <form:input path="lastName" id="lastName" type="text" class="form-control"
                                    placeholder="LastName"/>
                        <span style="color: red">${message}</span>
                        <label class="sr-only">Email </label>
                        <form:input path="id" id="email" type="email" class="form-control"
                                    placeholder="Email"/>

                        <label class="sr-only">Password</label>
                        <form:input path="password" id="password" type="password" class="form-control"
                                    placeholder="Password"/>

                        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign Up"/>


                        <div class="col-xs-12" id="gap-bottom">
                            <div class="text-center">By signing up, you agree to our <a href="#">Basic Rules</a>, <a
                                    href="#">Terms of Service</a>, and <a href="#">Privacy Policy.</a></div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /container -->
</body>
</html>
