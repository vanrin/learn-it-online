<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/css/maincssgui.css" rel="stylesheet">
</head>
<body>

<jsp:include page="navCourses.jsp"/>
<div class="container">

    <jsp:include page="headerCourses.jsp"/>
    <div class="row">
        <jsp:include page="menuCourses.jsp"/>

        <div class="col-sm-8 centerView">
            <div class="addSection">
                <div class="col-sm-8 formAddSection">

                    <form:form id="signupForm" action="../doAddSection" method="post" modelAttribute="section">

                        <div class="">

                            <form:input  path="id" id="title" type="hidden"
                                         disabled="disabled"  /> <br>
                        </div>
                        <span style="color: #0088cc">Title Section</span>
                        <div class="ipTitle">

                            <form:input  path="title" id="title" type="text" class="form-control title"
                                         placeholder="..." required="true"/> <br>
                        </div>
                        <br>
                        <br>
                        <span style="color: #0088cc">Goals Section</span>
                        <div class="">
                            <form:input  path="goalsSection" id="goalsSection" type="text" class="form-control title"
                                         placeholder="...." required="required"/> <br>
                        </div>
                        <br>
                        <br>
                        <span style="color: #0088cc">Description Section</span>
                        <div class="description" >
                            <form:textarea   class="form-control" path="description"    cols="70" rows="5"  required="required"/>
                            <br>
                        </div>
                        <div class="col-sm-8 " style="float: right;">
                            <input type="submit" style="margin-top: 40px;"  class="btn btn-success" name="" value="SAVE" placeholder="">
                        </div>
                        <form:hidden path="courseEntity.id"  />
                    </form:form>
                </div>
            </div>

        </div>
    </div>
</div>


<script src="jquery/mainj.js" type="text/javascript" ></script>
</body>
</html>