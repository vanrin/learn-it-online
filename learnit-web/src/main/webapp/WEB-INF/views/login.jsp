<%@ page import="com.limited.utils.FacebookOauthUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Register page based on Bootstrap 3.3.6">
    <meta name="author" content="ksud">
    <link rel="stylesheet" href="/bootstrap/bootstrap-3.3.6/css/bootstrap.css">
    <link href="/bootstrap/css/customlogin.css" rel="stylesheet">
    <link href="/bootstrap/css/theme.css" rel="stylesheet">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container formLogin"><!--container -->
    <div class="img-circle"  id="container-row">
        <div class="form-signin">
            <h3 class="form-signin-heading text-center">  <span style="color: #0088cc">Login to continue</span></h3>
            <div class="row">
                <div class="col-xs-6"><a href="<%=FacebookOauthUtil.getAuthorizationUrl()%>"><img src="bootstrap/images/fb.png" alt="Facebook" class="img-circle"></a>
                </div>

                <div class="col-xs-6"><a href="#"><img src="bootstrap/images/googleplus.png" alt="Google+"
                                                       class="img-circle pull-right"></a></div>
            </div>

            <div class="row">
                <div class="text-center"><span style="color: #0088cc">Or via email</span></div>
            </div>
            <div class="row" >
                <div class="text-center">${message}</div>
            </div>
            <div class="row">
                <form:form id="signinForm" action="checkLogin" method="post" modelAttribute="user">

                    <form:input id="id" path="id" type="id" class="form-control"
                                placeholder="Email"/><br>

                    <label class="sr-only">Password</label>
                    <form:input id="password" path="password" type="password" class="form-control"
                                placeholder="Password"></form:input>


                    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>

                </form:form>
                <div class="row">
                    <h3 class="form-signin-heading text-center">  <span style="color: #0088cc"><a href="/newUser" >Register</a></span></h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /container -->
</body>
</html>