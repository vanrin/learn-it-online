<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/css/maincssgui.css" rel="stylesheet">
</head>
<body>

<jsp:include page="navCourses.jsp"/>
<div class="container">

    <jsp:include page="headerCourses.jsp"/>
    <div class="row">
        <jsp:include page="menuCourses.jsp"/>

        <div class="col-sm-8 centerView">
            <div class="addLecture">
                <div class="col-sm-8 formAddLecture">
                    <form:form id="signupForm" action="../doAddSection" method="post" enctype="multipart/form-data"  modelAttribute="section">
                        <span>SECTION</span>


                            <form:input  path="id" id="title" type="hidden"
                                         disabled="disabled"  /> <br>

                        <div class="ipTitle">
                            <form:input  path="title" id="title" type="text" class="form-control title"
                                         placeholder="Enter a title" required="required"/> <br>
                        </div>
                        <div class="ipGoals">
                            <form:input  path="goalsSection" id="goalsSection" type="text" class="form-control "
                                         placeholder="Enter a Learning Objective" required="required"/> <br>
                        </div>

                        <span>LECTURE</span>
                        <!--<c:forEach items="${section.lectureEntities}" var="lecture" varStatus="status">-->
                            <div class="tittleLecture">
                                <form:input  path="lectureEntities[0].title" id="title" type="text"
                                             placeholder="Enter a Title" class="form-control "
                                             required="required"/> <br>
                            </div>

                            <div class="">
                                <form:input  path="lectureEntities[0].durationVideo" id="durationVideo"     placeholder="Enter duration video" type="text" class="form-control "
                                             required="required"/> <br>
                            </div>
                            <span style=" color: #0088cc">Upload Video</span>
                            <div class="">
                                <input  path="lectureEntities[0].linkVideo"  type="file"  name="file" value="video" class="btn btn-info"
                                             /> <br>
                            </div>
                            <span style="color: #0088cc">Upload Source</span>
                            <div class="sourceCode">
                                <input  path="lectureEntities[0].sourceCode"  type="file"  name="file" value="source" class="btn btn-info"
                                             /> <br>
                            <span style="color: #0088cc">Description</span>
                            <div class="description" >
                                <form:textarea   path="lectureEntities[0].description"    cols="70" rows="5"  required="required"/>
                                <br>
                            </div>
                       <!-- </c:forEach>-->

                        <div class="col-sm-8 " style="float: right;">
                            <input type="submit" style="margin-top: 20px;"  class="btn btn-success" name="" value="SAVE" placeholder="">
                        </div>

                                <form:hidden path="courseEntity.id"  />
                                <%--<input name="message" type="hidden" value="Hello Geeks!!"/>--%>

                    </form:form>

                </div>
            </div>

        </div>
    </div>
</div>

<script src="jquery/mainj.js" type="text/javascript" ></script>
</body>

</html>