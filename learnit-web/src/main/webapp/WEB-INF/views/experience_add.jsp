<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/css/maincssgui.css" rel="stylesheet">
</head>
<body>

<jsp:include page="navCourses.jsp"/>
<div class="container">
    <jsp:include page="headerCourses.jsp"/>
    <div class="row">
        <jsp:include page="menuCourses.jsp"/>

        <div class="col-sm-8 centerView">
            <div class="addLecture">
                <div class="col-sm-8 formAddLecture">
                    <span style="color: #0088cc"><h1>Experience</h1></span>
                    <form:form id="signupForm" action="../../experience/doAddExperience" method="post"
                               modelAttribute="experience">

                        <span style="color: #0088cc">Title</span>

                        <div class="">
                            <form:input path="title" id="title" type="text" placeholder="Enter a Title"
                                        class="form-control "
                                        required="required"/> <br>
                        </div>
                        <br>
                        <span style="color: #0088cc">Company</span>

                        <div class="">
                            <form:input path="company" id="title" type="text" placeholder="Enter a Title"
                                        class="form-control "
                                        required="required"/> <br>
                        </div>
                        <br>
                        <span style="color: #0088cc">Date From</span>

                        <div class="">
                            <form:input path="dateFrom" type="date"
                                        class="form-control "
                                        required="required"/> <br>
                        </div>
                        <br>
                        <span style="color: #0088cc">Date To</span>

                        <div class="">
                            <form:input path="dateTo" type="date"
                                        class="form-control "
                                        required="required"/> <br>
                        </div>
                        <br>
                        <span style="color: #0088cc">Description</span>

                        <div class="description">
                            <form:textarea path="description" class="form-control" cols="70" rows="5"
                                           required="required"/>
                            <br>
                        </div>

                        <div class="col-sm-8 " style="float: right;">
                            <input type="submit" style="margin-top: 20px;" class="btn btn-success" name="" value="SAVE"
                                   placeholder="">
                        </div>

                    </form:form>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="jquery/mainj.js" type="text/javascript"></script>
</body>
</html>