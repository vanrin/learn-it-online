<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/css/maincssgui.css" rel="stylesheet">
</head>
<body>

<jsp:include page="navCourses.jsp"/>
<div class="container">

    <jsp:include page="headerCourses.jsp"/>


    <div class="row">
        <jsp:include page="menuCourses.jsp"/>

        <div class="col-sm-8 centerView">

            <div class="homeSection">
                <p style="text-align: center; margin-top:10px; font-weight: bold;"><span style="color: #0088cc">SECTION HOME</span>
                </p>

                <div class="btAddSection">
                    <button type="button" onclick="location.href= '/section/newSection/${courseID}'"
                            class="btn btn-success" style="margin: 0px 0px 20px 0px; padding: 10px 40px 10px 40px">Add
                    </button>
                </div>

                <div class="tbHomePage">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="text-align: center"><span style="color: #0088cc">ID</span></th>
                            <th style="text-align: center"><span style="color: #0088cc">Title</span></th>
                            <th style="text-align: center"><span style="color: #0088cc">Goals </span></th>
                            <th style="text-align: center"><span style="color: #0088cc">Description</span></th>
                            <th ><span style="color: #0088cc">Action</span></th>
                            <th style="text-align: center"><span style="color: #0088cc">NOfLecture</span></th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="section" items="${listSection}">
                            <tr>
                                <td>${section.id}</td>
                                <td>${section.title}</td>
                                <td>${section.goalsSection}</td>
                                <td>${section.description}</td>
                                <td>
                                   <%-- <button class="btn btn-sm btn-danger"
                                            onclick="location.href='updateSection?id=${section.id}'">Edit
                                    </button>--%>
                                    <button class="btn btn-sm btn-danger"
                                            onclick="location.href='${section.id}'">Edit
                                    </button>
                                    <button class="btn btn-sm btn-danger"
                                            onclick="location.href='deleteSection?id=${section.id}'">Delete
                                    </button>
                                </td>
                                <td>
                                    number
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>


<script src="jquery/mainj.js" type="text/javascript"></script>
</body>
</html>