<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>LEARNIT EDU</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="academic/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
    <div id="topbar" class="clear">
        <!-- ################################################################################################ -->
        <nav>
            <ul>
                <li><a href="#">Home</a></li>

                <c:if test="${user==null}">
                    <li><a href="/loginUser">Login</a></li>
                    <li><a href="/newUser">Sign up</a></li>
                </c:if>
                <c:if test="${user!=null}">
                    <li><a href="/logout/logoutAccount">Log out</a></li>
                </c:if>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </nav>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
    <header id="header" class="clear">
        <!-- ################################################################################################ -->
        <div id="logo" class="fl_left">
            <h1><a href="index.html">LEARNIT EDU</a></h1>

        </div>
        <div class="fl_right">
            <form class="clear" method="post" action="#">
                <fieldset>
                    <legend>Search:</legend>
                    <input type="text" value="" placeholder="Search Here">
                    <button class="fa fa-search" type="submit" title="Search"><em>Search</em></button>
                </fieldset>
            </form>
        </div>
        <!-- ################################################################################################ -->
    </header>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row2">
    <div class="rounded">
        <nav id="mainav" class="clear">
            <!-- ################################################################################################ -->
            <ul class="clear">
                <li class="active"><a href="#">Home</a></li>
                <li><a class="drop" href="#">Courses</a>
                    <ul>
                        <li><a href="academic/pages/gallery.html">Hardware</a></li>
                        <li><a href="academic/pages/portfolio.html">Operating System</a></li>
                        <li><a href="academic/pages/full-width.html">Network And Security </a></li>
                        <li><a href="academic/pages/sidebar-left.html">Software</a></li>
                        <li><a href="academic/pages/sidebar-left-2.html">Other </a></li>

                    </ul>
                </li>

                <li><a href="/course/addCourse">Become an Instructor</a></li>
                <li><a href="#">Technology</a></li>
                <li><a href="#">Document</a></li>
                <li><a href="#">Application</a></li>
            </ul>
            <!-- ################################################################################################ -->
        </nav>
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper">
    <div id="slider">
        <div id="slide-wrapper" class="rounded clear">
            <!-- ################################################################################################ -->
            <figure id="slide-1"><a class="view" href="#"><img src="academic/images/demo/slider/home.jpg" alt=""></a>
                <figcaption>
                    <h2>follow excellence success will chase you.</h2>
                    <p class="right"><a href="#">Continue Reading &raquo;</a></p>
                </figcaption>
            </figure>
            <figure id="slide-2"><a class="view" href="#"><img src="academic/images/demo/slider/2.jpg" alt=""></a>

            </figure>
            <figure id="slide-3"><a class="view" href="#"><img src="academic/images/demo/slider/3.png" alt=""></a>

            </figure>
            <figure id="slide-4"><a class="view" href="#"><img src="academic/images/demo/slider/4.jpg" alt=""></a>

            </figure>
            <figure id="slide-5"><a class="view" href="#"><img src="academic/images/demo/slider/5.jpg" alt=""></a>

            </figure>
            <!-- ################################################################################################ -->
            <ul id="slide-tabs">
                <li><a href="#slide-1">All About The LEARNIT EDU</a></li>
                <li><a href="#slide-2">Why You Should Study With Us</a></li>
                <li><a href="#slide-3">Education And Student Experience</a></li>
                <li><a href="#slide-4">Project Of Student </a></li>
                <li><a href="#slide-5">Latest LEARTIT EDU News &amp; Events</a></li>
            </ul>
            <!-- ################################################################################################ -->
        </div>
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
    <div class="rounded">
        <main class="container clear">
            <!-- main body -->
            <!-- ################################################################################################ -->
            <div class="group btmspace-30">
                <!-- Left Column -->
                <div class="one_quarter first">
                    <!-- ################################################################################################ -->
                    <ul class="nospace">
                        <li class="btmspace-15"><a href="#"><em class="heading">Programming for Entrepreneurs - HTML & CSS</em>
                            <video width="210" height="100" controls >
                                <source src="media/androi.mp4"  type="video/mp4" media="">
                            </video>
                        </li>
                        <li class="btmspace-15"><a href="#"><em class="heading">Introduction to Web Development: HTML</em>
                            <video width="210" height="100" controls >
                                <source src="media/androi.mp4"  type="video/mp4" media="">
                            </video>
                        </li>
                        <li class="btmspace-15"><a href="#"><em class="heading">Introduction to PHP Programming Language</em>
                            <video width="210" height="100" controls >
                                <source src="media/androi.mp4"  type="video/mp4" media="">
                            </video>
                        </li>

                        </ul>
                    <!-- ################################################################################################ -->
                </div>
                <!-- / Left Column -->
                <!-- Middle Column -->
                <div class="one_half">
                    <!-- ################################################################################################ -->
                    <h2>Latest News &amp; Events</h2>
                    <ul class="nospace listing">
                        <li class="clear">
                            <div class="imgl borderedbox">
                                <video width="150" height="100" controls >
                                <source src="media/androi.mp4"  type="video/mp4" media="">
                                </video>
                            </div>
                            <p class="nospace btmspace-15"><a href="#">Java Essentials for Android.</a></p>
                            <p>This is a W3C compliant free website template from <a href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a>. This template is distributed using a <a href="http://www.os-templates.com/template-terms">Website Template Licence</a></p>
                        </li>
                        <li class="clear">
                            <div class="imgl borderedbox">
                                <video width="150" height="100" controls >
                                    <source src="media/androi.mp4"  type="video/mp4" media="">
                                </video>
                            </div>
                            <p class="nospace btmspace-15"><a href="#">The Complete Java Developer Course.</a></p>
                            <p>This is a W3C compliant free website template from <a href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a>. This template is distributed using a <a href="http://www.os-templates.com/template-terms">Website Template Licence</a></p>
                        </li>
                        <li class="clear">
                            <div class="imgl borderedbox">
                                <video width="150" height="100" controls >
                                    <source src="media/androi.mp4"  type="video/mp4" media="">
                                </video>
                            </div>
                            <p class="nospace btmspace-15"><a href="#">Android 6 - Master Android Marshmallow Development With Java.</a></p>
                            <p>This is a W3C compliant free website template from <a href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a>. This template is distributed using a <a href="http://www.os-templates.com/template-terms">Website Template Licence</a></p>
                        </li>
                    </ul>
                    <p class="right"><a href="#">Click here to view all of the latest news and events &raquo;</a></p>
                    <!-- ################################################################################################ -->
                </div>
                <!-- / Middle Column -->
                <!-- Right Column -->
                <div class="one_quarter sidebar">
                    <!-- ################################################################################################ -->
                    <div class="sdb_holder">
                        <h6>Virtual Tour</h6>
                        <div class="mediacontainer"><img src="academic/images/demo/video.gif" alt="">
                            <p><a href="#">View More Tour Videos Here</a></p>
                        </div>
                    </div>
                    <div class="sdb_holder">
                        <h6>Quick Information</h6>
                        <ul class="nospace quickinfo">
                            <li class="clear"><a href="#"><img src="academic/images/demo/80x80.gif" alt=""> Make An Application</a></li>
                            <li class="clear"><a href="#"><img src="academic/images/demo/80x80.gif" alt=""> Order A Prospectus</a></li>
                        </ul>
                    </div>
                    <!-- ################################################################################################ -->
                </div>
                <!-- / Right Column -->
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div id="twitter" class="group btmspace-30">
                <div class="one_quarter first center"><a href="#"><i class="fa fa-twitter fa-3x"></i><br>
                    Follow Us On Twitter</a></div>
                <div class="three_quarter bold">
                    <p></p>
                </div>
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div class="group">
                <h2></h2>
            </div>
            <!-- ################################################################################################ -->
            <!-- / main body -->
            <div class="clear"></div>
        </main>
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row4">
    <div class="rounded">
        <footer id="footer" class="clear">
            <!-- ################################################################################################ -->
            <div class="one_third first">
                <figure class="center"><img class="btmspace-15" src="academic/images/demo/worldmap.png" alt="">
                    <figcaption><a href="#">Find Us With Google Maps &raquo;</a></figcaption>
                </figure>
            </div>
            <div class="one_third">
                <address>
                    Long Educational Facility Name<br>
                    Address Line 2<br>
                    Town/City<br>
                    Postcode/Zip<br>
                    <br>
                    <i class="fa fa-phone pright-10"></i> xxxx xxxx xxxxxx<br>
                    <i class="fa fa-envelope-o pright-10"></i> <a href="#">contact@domain.com</a>
                </address>
            </div>
            <div class="one_third">
                <p class="nospace btmspace-10">Stay Up to Date With What's Happening</p>
                <ul class="faico clear">
                    <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="faicon-flickr" href="#"><i class="fa fa-flickr"></i></a></li>
                    <li><a class="faicon-rss" href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
                <form class="clear" method="post" action="#">
                    <fieldset>
                        <legend>Subscribe To Our Newsletter:</legend>
                        <input type="text" value="" placeholder="Enter Email Here&hellip;">
                        <button class="fa fa-sign-in" type="submit" title="Sign Up"><em>Sign Up</em></button>
                    </fieldset>
                </form>
            </div>
            <!-- ################################################################################################ -->
        </footer>
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
    <div id="copyright" class="clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy; 2014 - - <a href="#">FULLSTACK</a></p>
        <p class="fl_right"> <a target="_blank" href="#" title="Free Website Templates">LEARN IT</a></p>
        <!-- ################################################################################################ -->
    </div>
    </div>
</div>
<!-- JAVASCRIPTS -->
<script src="academic/layout/scripts/jquery.min.js"></script>
<script src="academic/layout/scripts/jquery.fitvids.min.js"></script>
<script src="academic/layout/scripts/jquery.mobilemenu.js"></script>
<script src="academic/layout/scripts/tabslet/jquery.tabslet.min.js"></script>
</body>
</html>
</html>