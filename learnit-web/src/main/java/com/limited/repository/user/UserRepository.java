package com.limited.repository.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.encrypt.AESCrypter;
import com.limited.entity.UserEntity;
import com.limited.template.ResponseTemplate;
import com.limited.utils.HttpRequester;
import com.limited.utils.HttpResult;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.security.GeneralSecurityException;


/**
 * Created by Administrator on 4/7/2016.
 */
@Repository
public class UserRepository {

    // Initialization vector
    private static String key = "abhY6^QuI90!t1nP";
    private static String iv = "Iay63!2Uy*)sQZhn";

    @Autowired
    HttpRequester httpRequester;

    @Autowired
    ObjectMapper objectMapper;

    ResponseTemplate responseTemplate = new ResponseTemplate();

    public UserEntity addUser(UserEntity userEntity) throws JsonProcessingException, GeneralSecurityException {

        String email = userEntity.getId();
        String password = userEntity.getPassword();
        //Encrypt email and password
        AESCrypter aesCrypter = new AESCrypter();

        String encryptEmail = aesCrypter.encrypt(email, key, iv);
        String encryptPassword = aesCrypter.encrypt(password, key, iv);

        userEntity.setId(encryptEmail);
        userEntity.setPassword(encryptPassword);
        String jsonBody = objectMapper.writeValueAsString(userEntity);

        HttpResult httpResult = httpRequester.sendPost("/user", jsonBody);

        if (httpResult.getStatusCode() == 200) {
            responseTemplate = objectMapper.treeToValue(httpResult.getRootJsonNode(), ResponseTemplate.class);
            // get status , if is 1 -> insert success into database
            if (responseTemplate.getStatus() == 1) {
                String jsonString = new JSONObject(responseTemplate.getData()).toString();
                try {
                    UserEntity user = objectMapper.readValue(jsonString, UserEntity.class);
                    return user;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean checkLogin(UserEntity userEntity) throws GeneralSecurityException, JsonProcessingException {


        String email = userEntity.getId();
        String password = userEntity.getPassword();
        //Encrypt email and password
        AESCrypter aesCrypter = new AESCrypter();

        String encryptEmail = aesCrypter.encrypt(email, key, iv);
        String encryptPassword = aesCrypter.encrypt(password, key, iv);

        userEntity.setId(encryptEmail);
        userEntity.setPassword(encryptPassword);
        String jsonBody = objectMapper.writeValueAsString(userEntity);

        HttpResult httpResult = httpRequester.sendPost("/user/login", jsonBody);

        return httpResult.getStatusCode() == 200;
    }

    public UserEntity addFacebookId(UserEntity userEntity) throws JsonProcessingException {

        // write object -> json and send restful
        String jsonBody = objectMapper.writeValueAsString(userEntity);
        HttpResult httpResult = httpRequester.sendPost("/user/registerVFacebook", jsonBody);
        // if send success return 200
        if (httpResult.getStatusCode() == 200) {
            // read json restful send
            responseTemplate = objectMapper.treeToValue(httpResult.getRootJsonNode(), ResponseTemplate.class);

            // status 1 when success
            if (responseTemplate.getStatus() == 1) {
                String jsonString = new JSONObject(responseTemplate.getData()).toString();
                try {
                    UserEntity user = objectMapper.readValue(jsonString, UserEntity.class);
                    return user;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }

    }
}
