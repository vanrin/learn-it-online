package com.limited.repository.course;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.entity.CourseEntity;
import com.limited.utils.HttpRequester;
import com.limited.utils.HttpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 4/25/2016.
 */
@Repository
public class CourseRepository {

    @Autowired
    HttpRequester httpRequester;

    @Autowired
    ObjectMapper objectMapper;

    public CourseEntity addCourse(CourseEntity courseEntity) throws JsonProcessingException {
        String jsonBody = objectMapper.writeValueAsString(courseEntity);
        HttpResult httpResult = httpRequester.sendPost("/course/addCourse", jsonBody);
        if (httpResult.getStatusCode() == 200) {

            JsonNode node = httpResult.getRootJsonNode();
            if (node.get("status").intValue() == 1) {
                CourseEntity course = objectMapper.treeToValue(node.get("data"), CourseEntity.class);
                return course;
            }
        } else {
            return null;
        }

        return null;
    }

    public boolean createGoalsCourse(CourseEntity courseEntity) throws JsonProcessingException {

        String jsonBody = objectMapper.writeValueAsString(courseEntity);
        HttpResult httpResult = httpRequester.sendPost("/course/goalsCourse", jsonBody);
        return httpResult.getStatusCode() == 200;
    }

    public List<CourseEntity> getListCourse() throws JsonProcessingException {

        List<CourseEntity> courseEntities = new ArrayList<>();
        HttpResult httpResult = httpRequester.sendGet("/course/listCourse");
        if (httpResult.getStatusCode() == 200) {
            JsonNode data = httpResult.getRootJsonNode();
            if (data.isArray() && data.size() != 0) {
                for (final JsonNode node : data) {
                    courseEntities.add(objectMapper.treeToValue(node, CourseEntity.class));
                }
            }
        }
        return courseEntities;
    }
}