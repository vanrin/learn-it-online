package com.limited.repository.aboutme;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.entity.CertificationEntity;
import com.limited.utils.HttpRequester;
import com.limited.utils.HttpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 5/11/2016.
 */
@Repository
public class CertificationRepository {

    @Autowired
    HttpRequester httpRequester;

    @Autowired
    ObjectMapper objectMapper;

    public boolean addCertification(CertificationEntity certificationEntity) throws JsonProcessingException {

        String jsonBody = objectMapper.writeValueAsString(certificationEntity);
        HttpResult httpResult = httpRequester.sendPost("/certification/addCertification", jsonBody);
        return httpResult.getStatusCode() == 200;
    }
}
