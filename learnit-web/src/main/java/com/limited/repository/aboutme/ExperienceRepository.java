package com.limited.repository.aboutme;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.entity.CertificationEntity;
import com.limited.entity.ExperienceEntity;
import com.limited.utils.HttpRequester;
import com.limited.utils.HttpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 5/13/2016.
 */
@Repository
public class ExperienceRepository {

    @Autowired
    HttpRequester httpRequester;
    @Autowired
    ObjectMapper objectMapper;

    public boolean addExperience(ExperienceEntity experienceEntity) throws JsonProcessingException {

        String jsonBody = objectMapper.writeValueAsString(experienceEntity);
        HttpResult httpResult = httpRequester.sendPost("/experience/addExperience", jsonBody);
        return httpResult.getStatusCode() == 200;
    }

}
