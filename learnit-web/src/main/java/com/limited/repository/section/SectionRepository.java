package com.limited.repository.section;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.limited.entity.SectionEntity;
import com.limited.utils.HttpRequester;
import com.limited.utils.HttpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 4/15/2016.
 */
@Repository
public class SectionRepository {


    @Autowired
    HttpRequester httpRequester;

    @Autowired
    ObjectMapper objectMapper;
    
    public boolean addSection(SectionEntity sectionEntity) throws IOException {

        String jsonBody = objectMapper.writeValueAsString(sectionEntity);
        HttpResult httpResult = httpRequester.sendPost("/section", jsonBody);
        return httpResult.getStatusCode() == 200;

    }

    public List<SectionEntity> getListSection(int courseID) throws JsonProcessingException {

        List<SectionEntity> sectionEntities = new ArrayList<>();
        HttpResult httpResult = httpRequester.sendGet("/section/listSection/" + courseID);

        if (httpResult.getStatusCode() == 200) {

            JsonNode data = httpResult.getRootJsonNode();
            if (data.isArray() && data.size() != 0) {
                for (final JsonNode node : data) {
                    sectionEntities.add(objectMapper.treeToValue(node, SectionEntity.class));
                }

            }
        }
        return sectionEntities;
    }

    public SectionEntity getSection(int sectionId) throws JsonProcessingException {

        SectionEntity sectionEntity = new SectionEntity();
        sectionEntity.setId(sectionId);
        String jsonBody = objectMapper.writeValueAsString(sectionEntity);
        HttpResult httpResult = httpRequester.sendPost("/section/getSection", jsonBody);
        if (httpResult.getStatusCode() == 200) {
            JsonNode node = httpResult.getRootJsonNode();
            if (node.get("status").intValue() == 1) {
                SectionEntity section = objectMapper.treeToValue(node.get("data"), SectionEntity.class);
                return section;
            }
        }
        return null;
    }

    public boolean updateSection(SectionEntity sectionEntity) throws JsonProcessingException {

        String jsonBody = objectMapper.writeValueAsString(sectionEntity);
        HttpResult httpResult = httpRequester.sendUpdate("/section/updateSection", jsonBody);

        return httpResult.getStatusCode() == 200;

    }

    public void deleteSection(int sectionId) {

        HttpResult httpResult = httpRequester.sendDelete("/section/deleteSection" + sectionId);

        if (httpResult.getStatusCode() == 200) {

        }
    }
}