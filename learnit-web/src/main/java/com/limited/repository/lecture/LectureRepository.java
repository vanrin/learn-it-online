package com.limited.repository.lecture;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.limited.entity.LectureEntity;
import com.limited.utils.HttpRequester;
import com.limited.utils.HttpResult;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by Administrator on 5/6/2016.
 */
@Repository
public class LectureRepository {

    @Autowired
    HttpRequester httpRequester;

    @Autowired
    ObjectMapper objectMapper;


    public boolean addLecture(LectureEntity lectureEntity, MultipartFile[] files) throws IOException {

        HttpClient httpclient = new DefaultHttpClient();
        httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

        HttpPost httppost = new HttpPost("http://localhost:8050/lecture/uploadFile");

        for (int i = 0; i < files.length; i++) {
            MultipartFile multipartFile = files[i];

            System.out.println(multipartFile.getOriginalFilename());

            if (multipartFile != null) {
                if (files[i].getOriginalFilename().length() > 0) {
                    if (files[i].getOriginalFilename().endsWith("rar")) {
                        lectureEntity.setSourceCode(files[i].getOriginalFilename());
                    } else {

                        lectureEntity.setLinkVideo(files[i].getOriginalFilename());
                    }
                    File file = new File(files[i].getOriginalFilename());

                    files[i].transferTo(file);

                    MultipartEntity mpEntity = new MultipartEntity();
                    ContentBody cbFile = new FileBody(file, "multipart/form-data");
                    mpEntity.addPart("file", cbFile);

                    httppost.setEntity(mpEntity);
                    HttpResponse response = null;
                    try {
                        response = httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    HttpEntity resEntity = response.getEntity();

                    if (resEntity != null) {
                        if (resEntity != null) {
                            resEntity.consumeContent();
                        }
                    }
                }

            }
        }
        httpclient.getConnectionManager().shutdown();

        String jsonBody = objectMapper.writeValueAsString(lectureEntity);
        System.out.println(jsonBody + "-------------------------------------------------------");
        HttpResult httpResult = httpRequester.sendPost("/lecture", jsonBody);


        return httpResult.getStatusCode() == 200;
    }

}
