package com.limited.utils;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpStatus;

public class HttpResult {

    private int statusCode;

    private JsonNode rootJsonNode;

    public HttpResult(int statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccessful() {
        return statusCode == HttpStatus.SC_OK;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public JsonNode getRootJsonNode() {
        return rootJsonNode;
    }

    public void setRootJsonNode(JsonNode rootJsonNode) {
        this.rootJsonNode = rootJsonNode;
    }
}