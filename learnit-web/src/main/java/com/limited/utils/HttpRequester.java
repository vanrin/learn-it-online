package com.limited.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;

@Component
public class HttpRequester {
    private String restServiceRootURI = "http://localhost:8050";
    private ObjectMapper mapper = new ObjectMapper();
    private CloseableHttpClient httpclient;

    public HttpRequester() {
        this.httpclient = HttpClients.createDefault();
    }

    public HttpResult sendGet(String url) {
        // example, call a GET request to http://localhost:8050/user
        HttpGet get = new HttpGet(restServiceRootURI + url);
        return executeRequest(get);
    }

    public HttpResult sendPost(String url, String body) {
        // example, call a POST request to http://localhost:8050/user
        HttpPost post = new HttpPost(restServiceRootURI + url);
        try {
            post.setEntity(new StringEntity(body));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //logger.error("post: " + e);
        }
        return executeRequest(post);
    }

    public HttpResult sendDelete(String url) {
        // example, call a DELETE request to http://localhost:8050/user/1
        HttpDelete delete = new HttpDelete(restServiceRootURI + url);
        return executeRequest(delete);
    }

    public HttpResult sendUpdate(String url, String body) {
        // example, call a UPDATE request to http://localhost:8050/user
        HttpPut put = new HttpPut(restServiceRootURI + url);
        try {
            put.setEntity(new StringEntity(body));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return executeRequest(put);
    }

    private HttpResult executeRequest(HttpUriRequest request) {
        HttpResult httpResult = new HttpResult(SC_NOT_FOUND);
        CloseableHttpResponse response = null;
        try {
            response = httpclient.execute(request);
            InputStream inputStream = response.getEntity().getContent();
            httpResult.setStatusCode(response.getStatusLine().getStatusCode());
            httpResult.setRootJsonNode(mapper.readTree(inputStream));
        } catch (IOException e) {
            //logger.error("Exception executing HTTP request", e);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    //logger.error("Exception closing response", e);
                }
            }
        }

        return httpResult;
    }
}
