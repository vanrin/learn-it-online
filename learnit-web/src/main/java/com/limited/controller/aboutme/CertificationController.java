package com.limited.controller.aboutme;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.limited.entity.CertificationEntity;
import com.limited.entity.UserEntity;
import com.limited.repository.aboutme.CertificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 5/11/2016.
 */
@Controller
@RequestMapping(value = "certification")
public class CertificationController {

    @Autowired
    CertificationRepository certificationRepository;

    @RequestMapping(value = "/addCertification")
    public String addCertification(Model model) {

        CertificationEntity certificationEntity = new CertificationEntity();
        model.addAttribute("certification", certificationEntity);
        return "certification_add";
    }

    @RequestMapping(value = "/doAddCertification")
    public String doAddCertification(@ModelAttribute CertificationEntity certificationEntity, HttpSession httpSession) throws JsonProcessingException {

        UserEntity userEntity = (UserEntity) httpSession.getAttribute("user");
        certificationEntity.setUserEntityC(userEntity);
        certificationRepository.addCertification(certificationEntity);
        return "/";
    }


}
