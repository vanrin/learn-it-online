package com.limited.controller.aboutme;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.limited.entity.ExperienceEntity;
import com.limited.entity.UserEntity;
import com.limited.repository.aboutme.ExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 5/13/2016.
 */
@Controller
@RequestMapping(value = "experience")
public class ExperienceController {

    @Autowired
    ExperienceRepository experienceRepository;

    @RequestMapping(value = "/addExperience")
    public String addExperience(Model model) {

        ExperienceEntity experienceEntity = new ExperienceEntity();
        model.addAttribute("experience", experienceEntity);
        return "experience_add";
    }

    @RequestMapping(value = "/doAddExperience")
    public String doAddExperience(@ModelAttribute ExperienceEntity experienceEntity, HttpSession httpSession) throws JsonProcessingException {

        UserEntity userEntity = (UserEntity) httpSession.getAttribute("user");
        experienceEntity.setUserEntityE(userEntity);
        experienceRepository.addExperience(experienceEntity);
        return "redirect:/certification/addCertification";
    }


}
