package com.limited.controller.section;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.limited.entity.CourseEntity;
import com.limited.entity.SectionEntity;
import com.limited.repository.course.CourseRepository;
import com.limited.repository.section.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

/**
 * Created by Administrator on 4/15/2016.
 */
@Controller
@RequestMapping(value = "section")
public class SectionController {

    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    CourseRepository courseRepository;

    @RequestMapping(value = "/updateSection")
    public String showUpdateSection(HttpServletRequest request, Model model) throws JsonProcessingException {

        int sectionId = Integer.parseInt(request.getParameter("id"));
        SectionEntity sectionEntity = sectionRepository.getSection(sectionId);
        if (sectionEntity != null) {
            model.addAttribute("sectionEdit", sectionEntity);
            return "section_edit";
        } else {
            return null;
        }

    }

    @RequestMapping(value = "/doUpdateSection", method = RequestMethod.POST)
    public String doUpdateSection(@ModelAttribute SectionEntity sectionEntity) throws JsonProcessingException, GeneralSecurityException {

        sectionRepository.updateSection(sectionEntity);
        return "redirect:/section/homeSection";
    }

    @RequestMapping(value = "/deleteSection", method = RequestMethod.GET)
    public String deleteSection(HttpServletRequest request) throws JsonProcessingException, GeneralSecurityException {

        int sectionId = Integer.parseInt(request.getParameter("id"));
        sectionRepository.deleteSection(sectionId);
        return "redirect:/section/homeSection";

    }

    @RequestMapping(value = "/homeSection/{courseID}", method = RequestMethod.GET)
    public String showHome(Model model, @PathVariable String courseID) throws JsonProcessingException {

        int idCourse = Integer.parseInt(courseID);
        List<SectionEntity> sectionEntityList = sectionRepository.getListSection(idCourse);
        model.addAttribute("listSection", sectionEntityList);
        model.addAttribute("courseID", courseID);

        return "section_home";
    }

    @RequestMapping(value = "/newSection/{courseID}", method = RequestMethod.GET)
    public String showNewSection(Model model, @PathVariable String courseID) {

        SectionEntity sectionEntity = new SectionEntity();
        CourseEntity entity = new CourseEntity();
        entity.setId(Integer.parseInt(courseID));
        sectionEntity.setCourseEntity(entity);
        model.addAttribute("section", sectionEntity);
        return "section_add";

    }

    @RequestMapping(value = "/doAddSection", method = RequestMethod.POST)
    public String doAddSection(@ModelAttribute SectionEntity sectionEntity) throws IOException, GeneralSecurityException {

        sectionRepository.addSection(sectionEntity);
        return "redirect:/section/homeSection/" + sectionEntity.getCourseEntity().getId();

    }


}
