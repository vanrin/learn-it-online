package com.limited.controller.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.limited.entity.UserEntity;
import com.limited.repository.user.UserRepository;
import com.limited.utils.FacebookOauthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by Administrator on 4/7/2016.
 */
@Controller
@RequestMapping(value = "/")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    public String showNewUser(Model model) {

        UserEntity userEntity = new UserEntity();
        model.addAttribute("user", userEntity);
        return "/register";
    }

    @RequestMapping(value = "/doAddUser", method = RequestMethod.POST)
    public String doAddUser(@ModelAttribute UserEntity userEntity, final RedirectAttributes redirectAttributes) throws JsonProcessingException, GeneralSecurityException {

        UserEntity userCheck = userRepository.addUser(userEntity);

        if (userCheck != null) {
            return "redirect:/";
        } else {
            redirectAttributes.addFlashAttribute("message", "Email existed.");
            return "redirect:/newUser";
        }

    }

    @RequestMapping(value = "/loginUser", method = RequestMethod.GET)
    public String loginUser(Model model) {

        UserEntity userEntity = new UserEntity();
        model.addAttribute("user", userEntity);
        return "/login";
    }

    @RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
    public String checkLogin(@ModelAttribute UserEntity userEntity, HttpSession session, Model model, final RedirectAttributes redirectAttributes) throws GeneralSecurityException, JsonProcessingException {

        userRepository.checkLogin(userEntity);
        if (userEntity != null) {
            session.setAttribute("user", userEntity);
            return "redirect:/";
        } else {
            redirectAttributes.addFlashAttribute("message", "Login fail.");
            model.addAttribute("user", new UserEntity());
            return "login";
        }
    }

    @RequestMapping(value = "/home")
    public String homePage() {
        return "/home";
    }

    @RequestMapping(value = "/serviceFacebook")
    public String service(HttpServletRequest request, HttpSession httpSession) throws IOException, GeneralSecurityException {
        String code = request.getParameter("code");
        if (code == null || code.isEmpty()) {
            throw new RuntimeException("ERROR: Didn't get code parameter in callback.");
        }

        OAuth20Service oAuth20Service = FacebookOauthUtil.getFBOauthService();
        final OAuth2AccessToken accessToken = oAuth20Service.getAccessToken(code);
        // get facebook id
        String facebookID = FacebookOauthUtil.getFacebookID(accessToken);
        UserEntity userEntity = new UserEntity();
        userEntity.setId(facebookID);
        UserEntity checkExistFacebookId = userRepository.addFacebookId(userEntity);

        if (checkExistFacebookId != null) {
            httpSession.setAttribute("user", checkExistFacebookId);
            return "redirect:/";
        } else {
            httpSession.setAttribute("user", userEntity);
            return "redirect:/";
        }
    }
}
