package com.limited.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 5/12/2016.
 */
@Controller
@RequestMapping(value = "logout")
public class Logout {

    @RequestMapping(value = "/logoutAccount", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) {

        HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:/";
    }
}
