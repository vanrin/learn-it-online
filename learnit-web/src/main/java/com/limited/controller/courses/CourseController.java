package com.limited.controller.courses;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.limited.entity.CourseEntity;
import com.limited.entity.UserEntity;
import com.limited.repository.course.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Administrator on 4/23/2016.
 */
@Controller
@RequestMapping(value = "/course")
public class CourseController {

    @Autowired
    CourseRepository courseRepository;

    @RequestMapping(value = "/basicCourse")
    public String basicCourse() {

        return "course_basic";
    }


    @RequestMapping(value = "/goalsCourse/{courseID}")
    public String goalsCourse(@PathVariable String courseID, Model model) {

        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setId(Integer.parseInt(courseID));
        model.addAttribute("courseEntity", courseEntity);
        model.addAttribute("courseID", courseID);
        return "goalsCourses";
    }

    @RequestMapping(value = "/doAddGoalsCourse", method = RequestMethod.POST)
    public String createGoalsCourse(@ModelAttribute CourseEntity courseEntity) throws JsonProcessingException {

        courseRepository.createGoalsCourse(courseEntity);
        return "redirect:/section/homeSection/" + courseEntity.getId();
    }

    @RequestMapping(value = "/addCourse", method = RequestMethod.GET)
    public String addCourse(Model model, HttpSession httpSession) {

        CourseEntity courseEntity = new CourseEntity();
        model.addAttribute("course", courseEntity);
        UserEntity entity = (UserEntity) httpSession.getAttribute("user");
        if (entity == null) {
            UserEntity userEntity = new UserEntity();
            model.addAttribute("user", userEntity);
            return "redirect:/loginUser";
        }
        return "course_add";
    }

    @RequestMapping(value = "/doAddCourse", method = RequestMethod.POST)
    public String createCourse(@ModelAttribute CourseEntity courseEntity, Model model, HttpSession httpSession) throws JsonProcessingException {

        UserEntity userEntity = (UserEntity) httpSession.getAttribute("user");
        courseEntity.setUserEntityCo(userEntity);
        CourseEntity entity = courseRepository.addCourse(courseEntity);

        if (entity != null) {

            return "redirect:/course/goalsCourse/" + entity.getId();
        } else {
            model.addAttribute("course", new CourseEntity());
            return "course_add";
        }

    }

    @RequestMapping(value = "/courseDetail", method = RequestMethod.GET)
    public String courseDetail(Model model) throws JsonProcessingException {

        List<CourseEntity> courseEntities = courseRepository.getListCourse();
        model.addAttribute("listCourse", courseEntities);
        return "course_detail";
    }
}
