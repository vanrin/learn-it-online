package com.limited.controller.lecture;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.limited.entity.LectureEntity;
import com.limited.entity.SectionEntity;
import com.limited.repository.lecture.LectureRepository;
import com.limited.repository.section.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

/**
 * Created by Administrator on 4/24/2016.
 */
@Controller
@RequestMapping(value = "lecture")
public class LectureController {

    @Autowired
    SectionRepository sectionRepository;
    @Autowired
    LectureRepository lectureRepository;

    @RequestMapping(value = "/createLecture/{courseID}", method = RequestMethod.GET)
    public String createLecture(Model model, @PathVariable String courseID) {

        int id = Integer.parseInt(courseID);
        LectureEntity lectureEntity = new LectureEntity();
        try {
            List<SectionEntity> sectionEntities = sectionRepository.getListSection(id);
            model.addAttribute("listSection", sectionEntities);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        model.addAttribute("lecture", lectureEntity);
        return "lecture_add";
    }

    @RequestMapping(value = "/doAddLecture", method = RequestMethod.POST)
    public String doAddLecture(@ModelAttribute LectureEntity lectureEntity, @RequestParam("file") MultipartFile[] file) throws IOException {

        System.out.println(lectureEntity.getSectionEntity().getId());
        lectureRepository.addLecture(lectureEntity, file);

       /* return "redirect:/section/homeSection";*/
        return "redirect:/experience/addExperience";
    }

}
