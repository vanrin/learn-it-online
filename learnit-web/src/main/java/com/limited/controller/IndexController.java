package com.limited.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 4/8/2016.
 */
@Controller
public class IndexController {

    @RequestMapping(value = {"/", "index"})
    public String indexPage() {
        return "index";
    }
}
