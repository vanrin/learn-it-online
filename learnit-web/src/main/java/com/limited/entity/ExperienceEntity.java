package com.limited.entity;


import java.util.Date;

/**
 * Created by Administrator on 5/10/2016.
 */

public class ExperienceEntity {

    private int id;
    private String title;
    private String company;
    private String dateFrom;
    private String dateTo;
    private String description;

    private UserEntity userEntityE;

    public ExperienceEntity() {

    }

    public UserEntity getUserEntityE() {
        return userEntityE;
    }

    public void setUserEntityE(UserEntity userEntityE) {
        this.userEntityE = userEntityE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
