package com.limited.entity;

/**
 * Created by Administrator on 4/24/2016.
 */
public class LectureEntity {

    private int id;
    private String title;
    private String durationVideo;
    private String linkVideo;
    private String sourceCode;
    private String description;
    private SectionEntity sectionEntity;

    public LectureEntity() {

    }

    public SectionEntity getSectionEntity() {
        return sectionEntity;
    }

    public void setSectionEntity(SectionEntity sectionEntity) {
        this.sectionEntity = sectionEntity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDurationVideo() {
        return durationVideo;
    }

    public void setDurationVideo(String durationVideo) {
        this.durationVideo = durationVideo;
    }

    public String getLinkVideo() {
        return linkVideo;
    }

    public void setLinkVideo(String linkVideo) {
        this.linkVideo = linkVideo;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
