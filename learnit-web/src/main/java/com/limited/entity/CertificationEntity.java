package com.limited.entity;


import java.util.Date;

/**
 * Created by Administrator on 5/10/2016.
 */

public class CertificationEntity {


    private int id;
    private String title;
    private String center;
    private String validFrom;
    private String validTo;
    private String description;

    private UserEntity userEntityC;

    public CertificationEntity() {

    }

    public UserEntity getUserEntityC() {
        return userEntityC;
    }

    public void setUserEntityC(UserEntity userEntityC) {
        this.userEntityC = userEntityC;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
