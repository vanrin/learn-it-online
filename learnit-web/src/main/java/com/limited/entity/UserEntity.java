package com.limited.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class UserEntity implements Serializable {


    private String id;
    private String firstName;
    private String lastName;
    private String password;
    private String phoneNumber;
    private String username;
    private String gender;
    private String birthday;
    private String about;
    private String image;

    private List<ExperienceEntity> experienceEntities = new ArrayList<>();

    private List<CertificationEntity> certificationEntities = new ArrayList<>();

    private List<CourseEntity> courseEntities = new ArrayList<>();

    public UserEntity() {

    }

    public List<CourseEntity> getCourseEntities() {
        return courseEntities;
    }

    public void setCourseEntities(List<CourseEntity> courseEntities) {
        this.courseEntities = courseEntities;
    }

    public List<CertificationEntity> getCertificationEntities() {
        return certificationEntities;
    }

    public void setCertificationEntities(List<CertificationEntity> certificationEntities) {
        this.certificationEntities = certificationEntities;
    }

    public List<ExperienceEntity> getExperienceEntities() {
        return experienceEntities;
    }

    public void setExperienceEntities(List<ExperienceEntity> experienceEntities) {
        this.experienceEntities = experienceEntities;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
