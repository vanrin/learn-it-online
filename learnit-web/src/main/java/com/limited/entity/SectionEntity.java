package com.limited.entity;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 4/15/2016.
 */
public class SectionEntity {

    private int id;
    private String title;
    private String goalsSection;
    private String description;
    private CourseEntity courseEntity;

    private List<LectureEntity> lectureEntities = new ArrayList<>();


    public SectionEntity() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGoalsSection() {
        return goalsSection;
    }

    public void setGoalsSection(String goalsSection) {
        this.goalsSection = goalsSection;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CourseEntity getCourseEntity() {
        return courseEntity;
    }

    public void setCourseEntity(CourseEntity courseEntity) {
        this.courseEntity = courseEntity;
    }

    public List<LectureEntity> getLectureEntities() {
        return lectureEntities;
    }

    public void setLectureEntities(List<LectureEntity> lectureEntities) {
        this.lectureEntities = lectureEntities;
    }
}
